/**
@license
Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/
import { LitElement, html } from 'lit-element';
import {resolveUrl} from '@cells-components/cells-lit-helpers/utils/resolve-url.js';
import {ifDefined} from 'lit-html/directives/if-defined';
/**
`cells-image` is an element for displaying an image that provides useful sizing and
preloading options not found on the standard `<img>` tag.

The `sizing` option allows the image to be either cropped (`cover`) or
letterboxed (`contain`) to fill a fixed user-size placed on the element.

The `preload` option prevents the browser from rendering the image until the
image is fully loaded.  In the interim, either the element's CSS `background-color`
can be be used as the placeholder, or the `placeholder` property can be
set to a URL (preferably a data-URI, for instant rendering) for an
placeholder image.

The `fade` option (only valid when `preload` is set) will cause the placeholder
image/color to be faded out once the image is rendered.

Examples:

  Basically identical to `<img src="...">` tag:

    <cells-image src="http://lorempixel.com/400/400"></cells-image>

  Will letterbox the image to fit:

    <cells-image style="width:400px; height:400px;" sizing="contain"
      src="http://lorempixel.com/600/400"></cells-image>

  Will crop the image to fit:

    <cells-image style="width:400px; height:400px;" sizing="cover"
      src="http://lorempixel.com/600/400"></cells-image>

  Will show light-gray background until the image loads:

    <cells-image style="width:400px; height:400px; background-color: lightgray;"
      sizing="cover" preload src="http://lorempixel.com/600/400"></cells-image>

  Will show a base-64 encoded placeholder image until the image loads:

    <cells-image style="width:400px; height:400px;" placeholder="data:image/gif;base64,..."
      sizing="cover" preload src="http://lorempixel.com/600/400"></cells-image>

  Will fade the light-gray background out once the image is loaded:

    <cells-image style="width:400px; height:400px; background-color: lightgray;"
      sizing="cover" preload fade src="http://lorempixel.com/600/400"></cells-image>

Custom property | Description | Default
----------------|-------------|----------
`--cells-image-placeholder` | Mixin applied to #placeholder | `{}`
`--cells-image-width` | Sets the width of the wrapped image | `auto`
`--cells-image-height` | Sets the height of the wrapped image | `auto`

* @customElement
* @demo demo/index.html
* @extends {LitElement}
*/
class CellsImage extends LitElement {
  static get is() {
    return 'cells-image';
  }

  static get properties() {
    return {
      /**
       * The URL of an image.
       */
      src: {
        type: String
      },

      /**
       * A short text alternative for the image.
       */
      alt: {
        type: String
      },

      /**
       * CORS enabled images support:
       * https://developer.mozilla.org/en-US/docs/Web/HTML/CORS_enabled_image
       */
      crossorigin: {
        type: String
      },

      /**
       * When true, the image is prevented from loading and any placeholder is
       * shown.  This may be useful when a binding to the src property is known to
       * be invalid, to prevent 404 requests.
       */
      preventLoad: {
        type: Boolean,
        attribute: 'prevent-load'
      },

      /**
       * Sets a sizing option for the image.  Valid values are `contain` (full
       * aspect ratio of the image is contained within the element and
       * letterboxed) or `cover` (image is cropped in order to fully cover the
       * bounds of the element), or `null` (default: image takes natural size).
       */
      sizing: {
        type: String,
        reflect: true
      },

      /**
       * When a sizing option is used (`cover` or `contain`), this determines
       * how the image is aligned within the element bounds.
       */
      position: {
        type: String
      },

      /**
       * When `true`, any change to the `src` property will cause the
       * `placeholder` image to be shown until the new image has loaded.
       */
      preload: {
        type: Boolean
      },

      /**
       * This image will be used as a background/placeholder until the src image
       * has loaded.  Use of a data-URI for placeholder is encouraged for instant
       * rendering.
       */
      placeholder: {
        type: String
      },

      /**
       * When `preload` is true, setting `fade` to true will cause the image to
       * fade into place.
       */
      fade: {
        type: Boolean
      },

      /**
       * Can be used to set the width of image (e.g. via binding); size may also
       * be set via CSS.
       */
      width: {
        type: Number
      },

      /**
       * Can be used to set the height of image (e.g. via binding); size may also
       * be set via CSS.
       */
      height: {
        type: Number
      },

      _loaded: {
        type: Boolean
      },

      _loading: {
        type: Boolean
      },

      _error: {
        type: Boolean
      }
    };
  }

  constructor() {
    super();
    this.src = '';
    this.preventLoad = false;
    this.position = 'center';
    this.preload = false;
    this.placeholder = null;
    this.fade = false;
    this._loaded = false;
    this._loading = false;
    this._error = false;
    this.width = null;
    this.height = null;
    this._resolvedSrc = '';
  }

  updated(changedProps) {
    if (changedProps.has('placeholder')) {
      this._placeholderChanged();
    }
    if (changedProps.has('width')) {
      this._widthChanged();
    }
    if (changedProps.has('height')) {
      this._heightChanged();
    }
    if (changedProps.has('sizing') || changedProps.has('position')) {
      this._transformChanged();
    }
    if (changedProps.has('src') || changedProps.has('preventLoad')) {
      this._loadStateObserver(this.src, this.preventLoad);
    }
  }

  render() {
    return html`
      <style>
        :host {
          display: inline-block;
          overflow: hidden;
          position: relative;
        }

        #baseURIAnchor {
          display: none;
        }

        #sizedImgDiv {
          position: absolute;
          top: 0px;
          right: 0px;
          bottom: 0px;
          left: 0px;
          display: none;
        }

        #img {
          display: block;
          width: var(--cells-image-width, auto);
          height: var(--cells-image-height, auto);
        }

        :host([sizing]) #sizedImgDiv {
          display: block;
        }

        :host([sizing]) #img {
          display: none;
        }

        #placeholder {
          position: absolute;
          top: 0px;
          right: 0px;
          bottom: 0px;
          left: 0px;
          background-color: inherit;
          opacity: 1;

        }

        #placeholder.faded-out {
          transition: opacity 0.5s linear;
          opacity: 0;
        }
      </style>

      <a id="baseURIAnchor" href="#"></a>
      <div id="sizedImgDiv" role="img"
        ?hidden="${this._computeImgDivHidden(this.sizing)}"
        aria-hidden="${ifDefined(this._computeImgDivARIAHidden(this.alt))}"
        aria-label="${ifDefined(this._computeImgDivARIALabel(this.alt, this.src))}">
      </div>
      <img id="img"
        alt="${ifDefined(this.alt)}"
        ?hidden="${this._computeImgHidden(this.sizing)}"
        crossorigin="${ifDefined(this.crossorigin)}"
        @load="${this._imgOnLoad}"
        @error="${this._imgOnError}">
      <div id="placeholder"
        ?hidden="${this._computePlaceholderHidden(this.preload, this.fade, this._loading, this._loaded)}"
        class="${this._computePlaceholderClassName(this.preload, this.fade, this._loading, this._loaded)}">
      </div>
    `;
  }

  /**
   * Read-only value that is true when the image is loaded.
   */
  get loaded() {
    return this._loaded;
  }

  /**
   * Read-only value that tracks the loading state of the image when the `preload` option is used.
   */
  get loading() {
    return this._loading;
  }

  /**
   * Read-only value that indicates that the last set `src` failed to load.
   */
  get error() {
    return this._error;
  }

  _setLoaded(value) {
    const oldValue = this._loaded;
    if (oldValue !== value) {
      this._loaded = value;
      /**
       * Fired when loaded value changes
       * @event loaded-changed
       */
      this.dispatchEvent(new CustomEvent('loaded-changed', {
        detail: {
          value: this._loaded
        }
      }));
    }
  }

  _setLoading(value) {
    const oldValue = this._loading;
    if (oldValue !== value) {
      this._loading = value;
      /**
       * Fired when loading value changes
       * @event loading-changed
       */
      this.dispatchEvent(new CustomEvent('loading-changed', {
        detail: {
          value: this._loading
        }
      }));
    }
  }

    /**
     * Fired when error value changes
     * @event error-changed
     */
  _setError(value) {
    const oldValue = this._error;
    if (oldValue !== value) {
      this._error = value;
      this.dispatchEvent(new CustomEvent('error-changed', {
        detail: {
          value: this._error
        }
      }));
    }
  }

  _imgOnLoad() {
    if (this.shadowRoot.querySelector('#img').src !== this._resolveSrc(this.src)) {
      return;
    }

    this._setLoading(false);
    this._setLoaded(true);
    this._setError(false);
  }

  _imgOnError() {
    if (this.shadowRoot.querySelector('#img').src !== this._resolveSrc(this.src)) {
      return;
    }

    this.shadowRoot.querySelector('#img').removeAttribute('src');
    this.shadowRoot.querySelector('#sizedImgDiv').style.backgroundImage = '';

    this._setLoading(false);
    this._setLoaded(false);
    this._setError(true);
  }

  _computePlaceholderHidden() {
    return !this.preload || (!this.fade && !this.loading && this.loaded);
  }

  _computePlaceholderClassName() {
    return (this.preload && this.fade && !this.loading && this.loaded) ? 'faded-out' : '';
  }

  _computeImgDivHidden() {
    return !this.sizing;
  }

  _computeImgDivARIAHidden() {
    return this.alt === '' ? 'true' : undefined;
  }

  _computeImgDivARIALabel() {
    if (this.alt !== null && this.alt !== undefined) {
      return this.alt;
    }

    // Polymer.ResolveUrl.resolveUrl will resolve '' relative to a URL x to that URL x, but '' is the default for src.
    if (this.src === '') {
      return '';
    }

    // NOTE: Use of `URL` was removed here because IE11 doesn't support constructing it. If this ends up being problematic, we should consider reverting and adding the URL polyfill as a dev dependency.
    let resolved = this._resolveSrc(this.src);
    // Remove query parts, get file name.
    return resolved ? resolved.replace(/[?|#].*/g, '').split('/').pop() : '';
  }

  _computeImgHidden() {
    return !!this.sizing;
  }

  _widthChanged() {
    this.style.width = isNaN(this.width) ? this.width : this.width + 'px';
  }

  _heightChanged() {
    this.style.height = isNaN(this.height) ? this.height : this.height + 'px';
  }

  _loadStateObserver(src, preventLoad) {
    var newResolvedSrc = this._resolveSrc(src);
    if (newResolvedSrc === this._resolvedSrc) {
      return;
    }

    this._resolvedSrc = '';
    this.shadowRoot.querySelector('#img').removeAttribute('src');
    this.shadowRoot.querySelector('#sizedImgDiv').style.backgroundImage = '';

    if (src === '' || preventLoad) {
      this._setLoading(false);
      this._setLoaded(false);
      this._setError(false);
    } else {
      this._resolvedSrc = newResolvedSrc;
      this.shadowRoot.querySelector('#img').src = this._resolvedSrc;
      this.shadowRoot.querySelector('#sizedImgDiv').style.backgroundImage =
          'url("' + this._resolvedSrc + '")';

      this._setLoading(true);
      this._setLoaded(false);
      this._setError(false);
    }
  }

  _placeholderChanged() {
    this.shadowRoot.querySelector('#placeholder').style.backgroundImage =
        this.placeholder ? 'url("' + this.placeholder + '")' : '';
  }

  _transformChanged() {
    var sizedImgDivStyle = this.shadowRoot.querySelector('#sizedImgDiv').style;
    var placeholderStyle = this.shadowRoot.querySelector('#placeholder').style;

    sizedImgDivStyle.backgroundSize = placeholderStyle.backgroundSize =
        this.sizing;

    sizedImgDivStyle.backgroundPosition = placeholderStyle.backgroundPosition =
        this.sizing ? this.position : '';

    sizedImgDivStyle.backgroundRepeat = placeholderStyle.backgroundRepeat =
        this.sizing ? 'no-repeat' : '';
  }

  _resolveSrc(testSrc) {
    const link = this.shadowRoot.querySelector('#baseURIAnchor');
    if (!link) {
      return;
    }
    let resolved = resolveUrl(testSrc, link.href);
    // NOTE: Use of `URL` was removed here because IE11 doesn't support constructing it. If this ends up being problematic, we should consider reverting and adding the URL polyfill as a dev dependency.
    if (resolved[0] === '/') {
      // In IE location.origin might not work
      // https://connect.microsoft.com/IE/feedback/details/1763802/location-origin-is-undefined-in-ie-11-on-windows-10-but-works-on-windows-7
      resolved = (location.origin || location.protocol + '//' + location.host) + resolved;
    }
    return resolved;
  }
}

customElements.define(CellsImage.is, CellsImage);
