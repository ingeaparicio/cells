import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers';
import { addListener as GesturesAddListener } from '@cells-components/cells-lit-helpers/utils/gestures.js';
import { ifDefined } from 'lit-html/directives/if-defined.js';
import { CellsFocusVisibleMixin } from '@cells-components/cells-focus-visible-mixin';
import styles from './cells-range-styles.js';
/**
 * Random unique ID for input
 * @param  {Number} length Length of random ID
 * @return {String}        Unique ID
 */
function uniqueID(length = 10) {
  return `_${Math.random().toString(36).substr(2, length)}`;
}

/**
`CellsRange` provides a base class as well as a `<cells-range>` custom element with input range functionality as close as possible to native range inputs. It provides an easier and convenient way to build range sliders with encapsulated HTML & custom styles. As it almost does not provide any styles, it's specially aimed to be extended.

As native range inputs, it can participate in forms in the same document using standard attributes, like 'form', 'name' and 'value'.

It fires 'change' and 'input' events, and provides 'stepUp' and 'stepDown' methods, as well as drag functionality for the slider knob, disabled & readonly states...

When extending from `CellsRange`, make sure to:
- Use `this._input` getter in render to enable communication with native forms.

**Example**
```js
// Extending CellsRange to create a CustomRange element
class CustomRange extends CellsRange {
  static get properties() {
    return {
      // Additional properties here
    };
  }

  static get styles() { // get styles from upper class using super
    return [
      super.styles,
      styles,
      getComponentSharedStyles('custom-range-shared-styles')
    ];
  }

  render() { // include any HTML you need; for example, icons
    return html`
      ...
      ${this._input}
    `;
  }
}

customElements.define('custom-range', CustomRange);
```

```html
<custom-range name="custom1" value="1"></custom-range>
<custom-range name="custom2" min="0" max="10" value="2" disabled></custom-range>
<custom-range name="custom3" min="3" max="12" step="3" value="3"></custom-range>
```

## FocusVisible mixin

`CellsRange` uses `FocusVisible mixin` from `@cells-components`. This means you can use the 'focus-visible' attribute on your component to manage ':focus' styles based on user navigation interface (keyboard/pointer).

```css
:host([focus-visible]) .knob {
  ...Your focus styles...
}
```

* @customElement cells-range
* @polymer
* @LitElement
* @demo demo/index.html
*/
export class CellsRange extends CellsFocusVisibleMixin(LitElement) {
  static get is() {
    return 'cells-range';
  }

  static get properties() {
    return {
      /**
       * Disabled state of input
       */
      disabled: {
        type: Boolean
      },

      /**
       * Input value
       */
      value: {
        type: Number
      },

      /**
       * Name of input related to forms
       */
      name: {
        type: String
      },

      /**
       * If true, input is readonly
       */
      readonly: {
        type: Boolean
      },

      /**
       * If true, autocomplete on input will be set to 'on'; otherwise, to 'off'
       */
      autocomplete: {
        type: Boolean
      },

      /**
       * Max attribute for input
       */
      max: {
        type: Number
      },

      /**
       * Min attribute for input
       */
      min: {
        type: Number
      },

      /**
       * step attribute for input
       */
      step: {
        type: Number
      },

      /**
       * Associated form for input
       */
      form: {
        type: String
      },

      _formElementId: {
        type: String
      }
    };
  }

  constructor() {
    super();
    this.name = '';
    this.min = 0;
    this.max = 100;
    this.step = 1;
    this._formElementId = uniqueID();
    this._onKeydown = this._onKeydown.bind(this);
    this.addEventListener('keydown', this._onKeydown);
  }

  connectedCallback() {
    super.connectedCallback();
    this._setInitialAttribute('role', 'slider');
    if (!this.disabled) {
      this._setInitialAttribute('tabindex', '0');
      this._setInitialAttribute('aria-disabled', 'false');
    }
    if (!this.readonly) {
      this._setInitialAttribute('aria-readonly', 'false');
    }
  }

  get min() {
    return this._min;
  }

  set min(val) {
    const oldVal = this._min;
    /* istanbul ignore else */
    if (oldVal !== val) {
      this._min = val;
      this.setAttribute('aria-valuemin', val);
      this.requestUpdate('min', oldVal);
    }
  }

  get max() {
    return this._max;
  }

  set max(val) {
    const oldVal = this._max;
    /* istanbul ignore else */
    if (oldVal !== val) {
      this._max = val;
      this.setAttribute('aria-valuemax', val);
      this.requestUpdate('max', oldVal);
    }
  }

  get value() {
    return this._value;
  }

  set value(val) {
    const oldVal = this._value;
    /* istanbul ignore else */
    if (oldVal !== val) {
      this._value = this._inMinMaxRange(val);
      this.setAttribute('aria-valuenow', this._value);
      this.requestUpdate('value', oldVal);
    }
  }

  get _innerValue() {
    return this._calcStep(this.value);
  }

  get _currentValue() {
    return this._inMinMaxRange(this._innerValue);
  }

  get _ratio() {
    return (this._currentValue - this.min) / (this.max - this.min);
  }

  get _percent() {
    return `${this._ratio * 100}%`
  }

  get _largeStep() {
    return Math.max((this.max - this.min) / 10, this.step);
  }

  update(changedProps) {
    if (!this._rendered && this.value === undefined) {
      this.value = this._calcStep((this.max - this.min) / 2 + this.min);
    }
    super.update(changedProps);
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this._rendered = true;
    this._bar = this.shadowRoot.querySelector('.bar');
    this._knob = this.shadowRoot.querySelector('.knob');
    this._formElement = this.shadowRoot.querySelector(`#${this._formElementId}`);
    /* istanbul ignore else */
    if (this._formElement) {
      this.appendChild(this._formElement);
    }
    GesturesAddListener(this._knob, 'track', ev => this._onTrack(ev));
    GesturesAddListener(this._knob, 'down', ev => this._onKnobDown(ev));
    if (this.hasAttribute('autofocus')) {
      this.focus();
    }
  }

  updated(changedProps) {
    if (changedProps.has('disabled')) {
      this.setAttribute('aria-disabled', this.disabled.toString());
      this.setAttribute('tabindex', this.disabled ? '-1' : '0');
    }
    if (changedProps.has('readonly')) {
      this.setAttribute('aria-readonly', this.readonly.toString());
    }
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-range-shared-styles')
    ];
  }

  render() {
    return html`
      ${this._controlBlock}
      ${this._input}
    `;
  }

  /**
   * Block containing HTML for bar and knob
   * @return {TemplateResult} HTML of bar and knob container
   */
  get _controlBlock() {
    return html`
      <div class="container" style="--_knob-left: ${this._percent};" @click="${this._barclick}" aria-hidden="true">
        ${this._barBlock}
        ${this._knobBlock}
      </div>
    `;
  }

  /**
   * Block containing HTML for bar
   * @return {TemplateResult} HTML of bar
   */
  get _barBlock() {
    return html`
      <div class="bar">
        <div class="bar-filled"></div>
      </div>
    `;
  }

  /**
   * Block containing HTML for knob
   * @return {TemplateResult} HTML of knob
   */
  get _knobBlock() {
    return html`
      <div class="current">
        <div class="knob">
          ${this._knobContent}
        </div>
      </div>
    `;
  }

  /**
   * Block containing HTML for knob content
   * @return {TemplateResult} HTML of knob content
   */
  get _knobContent() {
    return html``;
  }

  /**
   * HTML of native input for form communication
   * @return {TemplateResult} HTML of native input
   */
  get _input() {
    return html`
      <span class="slot">
        <slot name="_slider"></slot>
      </span>
      <input
        slot="_slider"
        type="range"
        id="${this._formElementId}"
        aria-hidden="true"
        tabindex="-1"
        autocomplete="${this.autocomplete ? 'on' : 'off'}"
        ?readonly="${this.readonly}"
        min="${this.min}"
        max="${this.max}"
        step="${this.step}"
        ?disabled="${this.disabled}"
        name="${ifDefined(this.name)}"
        .value="${this.value}"
        form="${ifDefined(this.form)}"
        @input="${this._onInputInput}">
      </input>
    `;
  }

  /**
   * Increments value in specified amount of steps, defaults to 1 step
   * @param {Number} val Steps added to value
   */
  stepUp(val = 1) {
    const newVal = this._inMinMaxRange(this.value + this.step * val);
    this.value = this._calcStep(newVal);
  }

  /**
   * Decrements value in specified amount of steps, defaults to 1 step
   * @param {Number} val Steps substracted to value
   */
  stepDown(val = 1) {
    const newVal = this._inMinMaxRange(this.value - this.step * val);
    this.value = this._calcStep(newVal);
  }

  _setInitialAttribute(attr, value) {
    /* istanbul ignore else */
    if (!this.hasAttribute(attr)) {
      this.setAttribute(attr, value);
    }
  }

  _inMinMaxRange(value) {
    return Math.max(this.min, Math.min(value, this.max));
  }

  _calcStep(value) {
    const val = parseFloat(value);

    if (!this.step) {
      return val;
    }

    const numSteps = Math.round((val - this.min) / this.step);
    if (this.step < 1) {
      /**
       * For small values of this.step, if we calculate the step using
       * `Math.round(value / step) * step` we may hit a precision point issue
       * eg. 0.1 * 0.2 =  0.020000000000000004
       * http://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html
       *
       * as a work around we can divide by the reciprocal of `step`
       */
      return numSteps / (1 / this.step) + this.min;
    } else {
      return numSteps * this.step + this.min;
    }
  }

  _onInputInput(ev) {
    this.value = ev.target.value;
  }

  _onKeydown(ev) {
    const key = ev.key.toLowerCase();
    switch(key) {
      case 'arrowleft':
      case 'arrowdown':
        this._keyUpdate(ev, () => this.stepDown());
        return;
      case 'arrowright':
      case 'arrowup':
        this._keyUpdate(ev, () => this.stepUp());
        return;
      case 'home':
        this._keyUpdate(ev, () => this.value = this._calcStep(this.min));
        return;
      case 'end':
        this._keyUpdate(ev, () => this.value = this._calcStep(this.max));
        return;
      case 'pagedown':
        this._keyUpdate(ev, () => this.stepDown(this._largeStep / this.step));
        return;
      case 'pageup':
        this._keyUpdate(ev, () => this.stepUp(this._largeStep / this.step));
        return;
      default:
        return;
    }
  }

  _keyUpdate(ev, cb) {
    if (!this.disabled && !this.readonly) {
      ev.preventDefault();
      const oldVal = this.value;
      cb();
      this._fireChanges(oldVal);
    }
  }

  _onKnobDown(ev) {
    if (!this.disabled) {
      ev.preventDefault();
      this.focus();
    }
  }

  _onTrack(ev) {
    if (!this.disabled && !this.readonly) {
      ev.preventDefault();
      ev.stopPropagation();
      switch (ev.detail.state) {
        case 'start':
          this._trackStart(ev);
          break;
        case 'track':
          this._trackX(ev);
          break;
        case 'end':
          this._trackEnd();
          break;
      }
    }
  }

  _trackStart() {
    this._knobWidth = this._knob.offsetWidth;
    this._barWidth = this._bar.offsetWidth - this._knobWidth;
    this._startX = this._barWidth * this._ratio;
    this._leftLimit = -this._startX;
    this._rightLimit = this._barWidth - this._startX;
    this._initialValue = this.value;
  }

  _trackX(ev) {
    const dx = Math.min(this._rightLimit, Math.max(this._leftLimit, ev.detail.dx));
    const currentPos = this._startX + dx;
    const knobPosition = (this.max - this.min) * currentPos / this._barWidth + this.min;
    const newValue = this._calcStep(knobPosition);
    if (newValue !== this.value) {
      this.value = newValue;
      this._fireEvent('input');
    }
  }

  _trackEnd() {
    if (this.value !== this._initialValue) {
      this._fireEvent('change');
    }
  }

  _barclick(ev) {
    if (!this.disabled && !this.readonly) {
      const rect = this._bar.getBoundingClientRect();
      const knobWidth = this._knob.offsetWidth;
      const barWidth = this._bar.offsetWidth - knobWidth;
      const currentPos = ev.clientX - rect.left - knobWidth / 2;
      const knobPosition = (this.max - this.min) * currentPos / barWidth + this.min;
      const newValue = this._calcStep(knobPosition);
      if (newValue !== this.value) {
        this.value = newValue;
        this._fireEvent('input');
        this._fireEvent('change');
      }
    }
  }

  _fireEvent(eventName) {
    this.dispatchEvent(new CustomEvent(eventName, {
      bubbles: true
    }));
  }

  _fireChanges(oldVal) {
    if (this.value !== oldVal) {
      this._fireEvent('input');
      this._fireEvent('change');
    }
  }
}

customElements.define(CellsRange.is, CellsRange);
