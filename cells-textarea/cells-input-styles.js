import { css, } from 'lit-element';

export default css`:host {
  box-sizing: border-box; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

.label {
  cursor: default; }
`;