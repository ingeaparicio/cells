/* eslint-disable new-cap */
/**
@license
Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at
http://polymer.github.io/LICENSE.txt The complete set of authors may be found at
http://polymer.github.io/AUTHORS.txt The complete set of contributors may be
found at http://polymer.github.io/CONTRIBUTORS.txt Code distributed by Google as
part of the polymer project is also subject to an additional IP rights grant
found at http://polymer.github.io/PATENTS.txt
*/
const animals = [
  'cat',
  'dog',
  'cow',
  'elephant',
  'iguana',
  'giraffe',
  'fox',
  'turtle',
  'lion',
  'duck',
  'horse'
];
import { LitElement } from 'lit-element';
import { CellsValidatorMixin } from '@cells-components/cells-validator-mixin/cells-validator-mixin.js';
class AnimalsOnly extends CellsValidatorMixin(LitElement) {
  static get is() {
    return 'animals-only';
  }
  validate(value) {
    return (
      value
        .split(',')
        .map(v => v.trim())
        .filter(v => !animals.includes(v)).length === 0
    );
  }
}
customElements.define(AnimalsOnly.is, AnimalsOnly);
