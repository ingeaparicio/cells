/**
@license
Copyright (c) 2015 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at
http://polymer.github.io/LICENSE.txt The complete set of authors may be found at
http://polymer.github.io/AUTHORS.txt The complete set of contributors may be
found at http://polymer.github.io/CONTRIBUTORS.txt Code distributed by Google as
part of the polymer project is also subject to an additional IP rights grant
found at http://polymer.github.io/PATENTS.txt
*/
import { LitElement, html } from 'lit-element';
import { getComponentSharedStyles } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import { ifDefined } from 'lit-html/directives/if-defined.js';
import { CellsDelegateAttributesMixin } from '@cells-components/cells-delegate-attributes-mixin';
import { CellsFocusVisibleMixin } from '@cells-components/cells-focus-visible-mixin';
import { CellsValidatableMixin } from '@cells-components/cells-validatable-mixin';
import { rafThrottle } from '@cells-components/cells-lit-helpers/utils/rafThrottle.js';
import styles from './cells-textarea-styles.js';

/**
`CellsTextarea` provides a base class as well as a `<cells-textarea>` custom element with textarea functionality as close as possible to native textareas. It provides an easier and convenient way to build textareas with encapsulated HTML & custom styles. As it almost does not provide any styles, it's specially aimed to be extended.

As native textareas, it can communicate with forms in the same document, as well as use aria attributes (for example, aria-haspopup) for relating with other elements in the same document. It can receive a 'disabled' state, receive an 'autofocus' attribute to get focus when component is attached to the document, and more.

`CellsTextarea` is inspired on [weightless.dev Textarea](https://weightless.dev/elements/textarea) for the form communication, and on [Iron Input](https://www.webcomponents.org/element/@polymer/iron-input) for validations management.

When extending from `CellsTextarea`, just make sure to:

- Call `CellsTextarea` styles using `super.styles`
- Interpolate `this.labelledTextarea` in render.

**Example**

```js
// Extending CellsTextarea to create a CustomTextarea element
class CustomTextarea extends CellsTextarea {
  static get properties() {
    return {
      // Additional properties here
    };
  }

  static get styles() {
    // be sure to get styles from upper class using super
    return [
      super.styles,
      styles,
      getComponentSharedStyles('custom-textarea-shared-styles')
    ];
  }

  render() {
    // include any HTML you need; for example, icons
    return html`
      ${this.labelledTextarea}
    `;
  }
}
customElements.define('custom-textarea', CustomTextarea);
```

* @customElement cells-textarea
* @extends {LitElement}
* @appliesMixin CellsFocusVisibleMixin
* @appliesMixin CellsValidatableMixin
* @appliesMixin CellsDelegateAttributesMixin
* @demo demo/index.html
*/
export class CellsTextarea extends CellsValidatableMixin(
  CellsDelegateAttributesMixin(CellsFocusVisibleMixin(LitElement))
) {
  static get is() {
    return 'cells-textarea';
  }

  static get properties() {
    return {
      /**
       * Text which the textarea will get through an aria-describedby attribute
       */
      accessibleDescription: {
        type: String,
        attribute: 'accessible-description'
      },
      /**
       * Set to true to auto-validate the textarea value.
       */
      autoValidate: {
        type: Boolean,
        attribute: 'auto-validate'
      },
      /**
       * If true, autocomplete on textarea will be set to 'on'; otherwise, to 'off'
       */
      autocomplete: {
        type: Boolean
      },
      /**
       * Disabled state of textarea
       */
      disabled: {
        type: Boolean,
        reflect: true
      },
      /**
       * Associated form for textarea
       */
      form: {
        type: String
      },
      /**
       * Label for the textarea
       */
      label: {
        type: String
      },
      /**
       * maxlength attribute for textarea
       */
      maxlength: {
        type: Number
      },
      /**
       * minlength attribute for textarea
       */
      minlength: {
        type: Number
      },
      /**
       * Name of textarea related to forms
       */
      name: {
        type: String
      },
      /**
       * Specifies a short hint that describes the expected value of a textarea
       */
      placeholder: {
        type: String
      },
      /**
       * If true, textarea is readonly
       */
      readonly: {
        type: Boolean
      },
      /**
       * If true, textarea is required
       */
      required: {
        type: Boolean
      },
      /**
       * Textarea value
       */
      value: {
        type: String,
        reflect: true
      },
      _formElementId: {
        type: String
      }
    };
  }

  constructor() {
    super();
    this.label = '';
    this.autoValidate = false;
    this._formElementId = this.uniqueID();
    this._onInputRaf = rafThrottle(this._onInput);
  }

  /**
   * List of attributes which will get delegated to native textarea
   */
  get delegatedAttributes() {
    return [
      'aria-activedescendant',
      'aria-autocomplete',
      'aria-multiline',
      'aria-placeholder',
      'aria-controls',
      'aria-describedby',
      'aria-flowto',
      'aria-haspopup',
      'aria-invalid',
      'aria-labelledby',
      'aria-owns',
      'inputmode'
    ];
  }

  /**
   * Target node of attribute delegation
   */
  get delegatedAttributesTarget() {
    return this._formElement;
  }

  /**
   * Random unique ID for textarea
   * @param  {Number} length Length of random ID
   * @return {String}        Unique ID
   */
  uniqueID(length = 10) {
    return `_${Math.random()
      .toString(36)
      .substr(2, length)}`;
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this._formElement = this.shadowRoot.querySelector(
      `#${this._formElementId}`
    );
    this._moveToLight(this._formElement);
    this._a11yDescription = this.shadowRoot.querySelector(
      `#description${this._formElementId}`
    );
    this._moveToLight(this._a11yDescription);
    super.firstUpdated(changedProps);
    if (this.hasAttribute('autofocus')) {
      this.focus();
    }
  }

  updated(changedProps) {
    super.updated(changedProps);
    if (changedProps.has('value') && this.autoValidate) {
      this.validate();
    }
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-textarea-shared-styles')
    ];
  }

  render() {
    return html`
      ${this.labelledTextarea}
    `;
  }

  /**
   * HTML block containing visible label, textarea slot and textarea
   * @return {TemplateResult} HTML with label, textarea slot and textarea
   */
  get labelledTextarea() {
    return html`
      ${this.labelBlock}
      <span class="slot-textarea">
        <slot name="_textarea"></slot>
      </span>
      ${this._textarea}
    `;
  }

  /**
   * HTML block containing label span
   * @return {TemplateResult} HTML with label
   */
  get labelBlock() {
    return html`
      <span class="label" aria-hidden="true" @click="${this.focus}">
        ${this.textareaLabel}
      </span>
    `;
  }

  /**
   * Returns text to be used as textarea aria-label
   * @return {String} aria-label text for textarea
   */
  get textareaLabel() {
    return this.label;
  }

  /**
   * Native textarea to be moved to light DOM for communication with native forms and ARIA relationships
   * @return {TemplateResult} HTML of native textarea
   */
  get _textarea() {
    return html`
      <textarea
        slot="_textarea"
        id="${this._formElementId}"
        ?disabled="${this.disabled}"
        name="${ifDefined(this.name)}"
        .value="${this._value}"
        ?required="${this.required}"
        ?readonly="${this.readonly}"
        autocomplete="${this.autocomplete ? 'on' : 'off'}"
        aria-label="${this.textareaLabel}"
        aria-describedby="description${this._formElementId}"
        aria-multiline="true"
        maxlength="${ifDefined(this.maxlength)}"
        minlength="${ifDefined(this.minlength)}"
        placeholder="${ifDefined(this.placeholder)}"
        form="${ifDefined(this.form)}"
        @input="${this._onInputRaf}"
        @keypress="${this._onKeyPress}"
      ></textarea>
      <span
        style="display: none;"
        slot="_textarea"
        id="description${this._formElementId}"
      >
        ${this.accessibleDescription}
      </span>
    `;
  }

  get _value() {
    return this.value ? this.value : '';
  }

  get _valueSize() {
    return this._value.length;
  }

  /**
   * Method for getting value from textarea
   */
  _onInput(e) {
    this.value = e.target.value.trim();
  }

  /**
   * Method when borwser registers keyboard input.
   */
  _onKeyPress(e) {
    return;
  }

  /**
   * Method for passing blur to native textarea
   */
  blur() {
    this._formElement.blur();
  }

  /**
   * Method for passing focus to native textarea
   */
  focus() {
    this._formElement.focus();
  }

  /**
   * Method for passing select to native textarea
   */
  select() {
    this._formElement.select();
  }

  /**
   * Method for passing setRangeText to native textarea
   */
  setRangeText(...args) {
    this._formElement.setRangeText(...args);
  }

  /**
   * Method for passing setSelectionRange to native textarea
   */
  setSelectionRange(...args) {
    this._formElement.setSelectionRange(...args);
  }

  _moveToLight(element) {
    if (element) {
      this.appendChild(element);
    }
  }

  /**
   * Returns true if `value` is valid. The validator provided in `validator` will be used first, then any constraints.
   * @return {boolean} True if the value is valid.
   */
  validate() {
    // Use the nested textarea's native validity.
    let valid = this._formElement.checkValidity();

    if (
      valid &&
      ((this.maxlength && this._valueSize > this.maxlength) ||
        (this.minlength &&
          this._valueSize > 0 &&
          this._valueSize < this.minlength))
    ) {
      valid = false;
    }

    if (valid && this.hasValidator()) {
      valid = super.validate(this.value);
    }

    this.invalid = !valid;
    /**
     * Fired whenever `validate()` is called.
     * @event cells-textarea-validate
     * @param {Boolean} detail True if element is valid
     */
    this.dispatchEvent(
      new CustomEvent('cells-textarea-validate', {
        bubbles: true,
        composed: true,
        detail: valid
      })
    );

    return valid;
  }
}

customElements.define(CellsTextarea.is, CellsTextarea);
