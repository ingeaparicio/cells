/* eslint-disable new-cap */
/**
@license
Copyright (c) 2015 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at
http://polymer.github.io/LICENSE.txt The complete set of authors may be found at
http://polymer.github.io/AUTHORS.txt The complete set of contributors may be
found at http://polymer.github.io/CONTRIBUTORS.txt Code distributed by Google as
part of the polymer project is also subject to an additional IP rights grant
found at http://polymer.github.io/PATENTS.txt
*/
import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers';
import { ifDefined } from 'lit-html/directives/if-defined.js';
import { CellsDelegateAttributesMixin } from '@cells-components/cells-delegate-attributes-mixin';
import { CellsValidatableMixin } from '@cells-components/cells-validatable-mixin';
import { CellsFocusVisibleMixin } from '@cells-components/cells-focus-visible-mixin';
import styles from './cells-input-styles.js';
/**
`CellsInput` provides a base class as well as a `<cells-input>` custom element with input functionality as close as possible to native inputs. It provides an easier and convenient way to build inputs with encapsulated HTML & custom styles. As it almost does not provide any styles, it's specially aimed to be extended.

As native inputs, it can communicate with forms in the same document, as well as use aria attributes (for example, aria-haspopup) for relating with other elements in the same document. It can receive any valid 'type' for inputs ('text', 'number', 'search'...), a 'disabled' state, receive an 'autofocus' attribute to get focus when component is attached to the document, and more.

`CellsInput` is inspired on [weightless.dev Textfield](https://weightless.dev/elements/textfield) for the form communication, and on [Iron Input](https://www.webcomponents.org/element/@polymer/iron-input) for validations management.

When extending from `CellsInput`, just make sure to:
- Call `CellsInput` styles using `super.styles`
- Interpolate `this.labelledInput` in render.

## Custom validators

`CellsInput` extends `CellsValidatableMixin`. You can use custom validators that implement `CellsValidatorMixin` with `<cells-input>`.

```html
<cells-input auto-validate validator="my-custom-validator"></cells-input>
```

## Stopping invalid input

It may be desirable to only allow users to enter certain characters. You can use the `allowed-pattern` attribute to accomplish this. This feature is separate from validation, and `allowed-pattern` does not affect how the input is validated.

```html
<!-- only allow characters that match [0-9], and check in validation that there are 5 digits -->
<cells-input allowed-pattern="[0-9]" pattern="\d{5}"></cells-input>
```

**Example**
```js
// Extending CellsInput to create a CustomInput element
class CustomInput extends CellsInput {
  static get properties() {
    return {
      // Additional properties here
    };
  }

  static get styles() { // be sure to get styles from upper class using super
    return [
      super.styles,
      styles,
      getComponentSharedStyles('custom-input-shared-styles')
    ];
  }

  render() { // include any HTML you need; for example, icons
    return html`
      ${this.labelledInput}
    `;
  }
}
customElements.define('custom-input', CustomInput)
```

```html
<custom-input type="number" label="Enter number"></custom-input>
<custom-input disabled label="Disabled input"></custom-input>
<custom-input @input="${this._myCallback}" label="With input listener on lit-html"></custom-input>
```

* @customElement
* @demo demo/index.html
* @extends {LitElement}
* @appliesMixin CellsValidatableMixin
* @appliesMixin CellsDelegateAttributesMixin
* @appliesMixin CellsFocusVisibleMixin
*/
export class CellsInput extends CellsValidatableMixin(CellsDelegateAttributesMixin(CellsFocusVisibleMixin(LitElement))) {

  static get is() {
    return 'cells-input';
  }

  static get properties() {
    return {
      /**
       * Type for the input
       */
      type: {
        type: String,
        reflect: true
      },
      /**
       * Disabled state of input
       */
      disabled: {
        type: Boolean,
        reflect: true
      },
      /**
       * Input value
       */
      value: {
        type: String
      },
      /**
       * Name of input related to forms
       */
      name: {
        type: String
      },
      /**
       * If true, input is required
       */
      required: {
        type: Boolean
      },
      /**
       * If true, input is readonly
       */
      readonly: {
        type: Boolean
      },
      /**
       * If true, autocomplete on input will be set to 'on'; otherwise, to 'off'
       */
      autocomplete: {
        type: Boolean
      },
      /**
       * Label for the input
       */
      label: {
        type: String
      },
      /**
       * Regexp for native input pattern attribute, checked on form submit or validate()
       */
      pattern: {
        type: String
      },
      /**
       * Regex-like list of characters allowed as input; all characters not in the
       * list will be rejected. The recommended format should be a list of allowed
       * characters, for example, `[a-zA-Z0-9.+-!;:]`.
       *
       * This pattern represents the allowed characters for the field; as the user
       * inputs text, each individual character will be checked against the
       * pattern (rather than checking the entire value as a whole). If a
       * character is not a match, it will be rejected.
       *
       * Pasted input will have each character checked individually; if any
       * character doesn't match `allowedPattern`, the entire pasted string will
       * be rejected.
       */
      allowedPattern: {
        type: String,
        attribute: 'allowed-pattern'
      },
      /**
       * Max attribute for input
       */
      max: {
        type: String
      },
      /**
       * maxlength attribute for input
       */
      maxlength: {
        type: Number
      },
      /**
       * Min attribute for input
       */
      min: {
        type: String
      },
      /**
       * minlength attribute for input
       */
      minlength: {
        type: Number
      },
      /**
       * step attribute for input
       */
      step: {
        type: Number
      },
      /**
       * Text which the input will get through an aria-describedby attribute
       */
      accessibleDescription: {
        type: String,
        attribute: 'accessible-description'
      },
      /**
       * Set to true to auto-validate the input value as you type.
       */
      autoValidate: {
        type: Boolean,
        attribute: 'auto-validate'
      },
      /**
       * Associated form for input
       */
      form: {
        type: String
      },
      _formElementId: {
        type: String
      }
    };
  }

  constructor() {
    super();
    this.type = 'text';
    this.label = '';
    this.autoValidate = false;
    this._previousValidInput = '';
    this._patternAlreadyChecked = false;
    this._formElementId = this.uniqueID();
  }

  /**
   * List of attributes which will get delegated to native input
   */
  get delegatedAttributes() {
    return [
      'aria-activedescendant',
      'aria-autocomplete',
      'aria-controls',
      'aria-describedby',
      'aria-flowto',
      'aria-haspopup',
      'aria-invalid',
      'aria-labelledby',
      'aria-owns',
      'inputmode'
    ];
  }

  /**
   * Target node of attribute delegation
   */
  get delegatedAttributesTarget() {
    return this._formElement;
  }

  /**
   * Random unique ID for input
   * @param  {Number} length Length of random ID
   * @return {String}        Unique ID
   */
  uniqueID(length = 10) {
    return `_${Math.random().toString(36).substr(2, length)}`;
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this._formElement = this.shadowRoot.querySelector(`#${this._formElementId}`);
    this._moveToLight(this._formElement);
    this._a11yDescription = this.shadowRoot.querySelector(`#description${this._formElementId}`);
    this._moveToLight(this._a11yDescription);
    super.firstUpdated(changedProps);
    if (this.hasAttribute('autofocus')) {
      this.focus();
    }
  }

  updated(changedProps) {
    super.updated(changedProps);
    if (changedProps.has('value') && this.autoValidate) {
      this.validate();
    }
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-input-shared-styles')
    ];
  }

  render() {
    return html`
      ${this.labelledInput}
    `;
  }

  /**
   * HTML block containing visible label, input slot and input
   * @return {TemplateResult} HTML with label, input slot and input
   */
  get labelledInput() {
    return html`
      ${this.labelBlock}
      ${this._input}
    `;
  }

  /**
   * HTML block containing label span
   * @return {TemplateResult} HTML with label
   */
  get labelBlock() {
    return html`
      <span class="label" aria-hidden="true" @click="${this.focus}">${this.inputLabel}</span>
    `;
  }

  /**
   * Returns text to be used as input aria-label
   * @return {String} aria-label text for input
   */
  get inputLabel() {
    return this.label;
  }

  /**
   * Native input to be moved to light DOM for communication with native forms and ARIA relationships
   * @return {TemplateResult} HTML of native input
   */
  get _input() {
    return html`
      <span class="slot-input">
        <slot name="_input"></slot>
      </span>
      <input
        id="${this._formElementId}"
        slot="_input"
        type="${this.type}"
        ?disabled="${this.disabled}"
        name="${ifDefined(this.name)}"
        .value="${ifDefined(this.value)}"
        ?required="${this.required}"
        ?readonly="${this.readonly}"
        autocomplete="${this.autocomplete ? 'on' : 'off'}"
        aria-label="${this.inputLabel}"
        aria-describedby="description${this._formElementId}"
        max="${ifDefined(this.max)}"
        min="${ifDefined(this.min)}"
        maxlength="${ifDefined(this.maxlength)}"
        minlength="${ifDefined(this.minlength)}"
        step="${ifDefined(this.step)}"
        pattern="${ifDefined(this.pattern)}"
        @input="${this._onInputInput}"
        @keypress="${this._onInputKeypress}"
        form="${ifDefined(this.form)}"
      >
      <span style="display: none;" slot="_input" id="description${this._formElementId}">${this.accessibleDescription}</span>
    `;
  }

  /**
   * Method for passing focus to native input
   */
  focus() {
    this._formElement.focus();
  }

  /**
   * Method for passing blur to native input
   */
  blur() {
    this._formElement.blur();
  }

  /**
   * Method for passing select to native input
   */
  select() {
    this._formElement.select();
  }

  /**
   * Method for passing setSelectionRange to native input
   */
  setSelectionRange(...args) {
    this._formElement.setSelectionRange(...args);
  }

  /**
   * Method for passing setRangeText to native input
   */
  setRangeText(...args) {
    this._formElement.setRangeText(...args);
  }

  _moveToLight(element) {
    if (element) {
      this.appendChild(element);
    }
  }

  _onInputInput(ev) {
    const maxLength = this.maxlength;
    const form = this._formElement;

    if (maxLength && form.value.length > maxLength) {
      form.value = form.value.slice(0, maxLength);
    }
    this.value = form.value;

    // Need to validate each of the characters pasted if they haven't been validated inside `_onKeypress` already.
    if (this.allowedPattern && !this._patternAlreadyChecked) {
      const valid = this._checkPatternValidity();
      if (!valid) {
        this.value = this._previousValidInput;
      }
    }
    this._previousValidInput = this.value;
    this._patternAlreadyChecked = false;
  }

  _onInputKeypress(ev) {
    if (!this.allowedPattern && this.type !== 'number') {
      return;
    }
    const regexp = this._patternRegExp;
    if (!regexp) {
      return;
    }

    // Handle special keys and backspace
    if (ev.metaKey || ev.ctrlKey || ev.altKey) {
      return;
    }

    // Check the pattern either here or in `_onInput`, but not in both.
    this._patternAlreadyChecked = true;

    const thisChar = String.fromCharCode(ev.charCode);
    if (this._isPrintable(ev) && !regexp.test(thisChar)) {
      ev.preventDefault();
    }
  }

  _checkPatternValidity() {
    const regexp = this._patternRegExp;
    if (!regexp) {
      return true;
    }
    for (var i = 0; i < this.value.length; i++) {
      if (!regexp.test(this.value[i])) {
        return false;
      }
    }
    return true;
  }

  get _patternRegExp() {
    let pattern;
    if (this.allowedPattern) {
      pattern = new RegExp(this.allowedPattern);
    } else {
      switch (this.type) {
        case 'number':
          pattern = /[0-9.,e-]/;
          break;
      }
    }
    return pattern;
  }

  /* eslint-disable complexity, eqeqeq */
  _isPrintable(ev) {
    // What a control/printable character is varies wildly based on the browser.
    // - most control characters (arrows, backspace) do not send a `keypress` event in Chrome, but the *do* on Firefox
    // - in Firefox, when they do send a `keypress` event, control chars have a charCode = 0, keyCode = xx (for ex. 40 for down arrow)
    // - printable characters always send a keypress event.
    // - in Firefox, printable chars always have a keyCode = 0. In Chrome, the keyCode always matches the charCode.
    // None of this makes any sense.

    // For these keys, ASCII code == browser keycode.
    const anyNonPrintable = (ev.keyCode == 8) ||  // backspace
        (ev.keyCode == 9) ||                    // tab
        (ev.keyCode == 13) ||                   // enter
        (ev.keyCode == 27);                     // escape

    // For these keys, make sure it's a browser keycode and not an ASCII code.
    const mozNonPrintable = (ev.keyCode == 19) ||  // pause
        (ev.keyCode == 20) ||                    // caps lock
        (ev.keyCode == 45) ||                    // insert
        (ev.keyCode == 46) ||                    // delete
        (ev.keyCode == 144) ||                   // num lock
        (ev.keyCode == 145) ||                   // scroll lock
        (ev.keyCode > 32 &&
         ev.keyCode < 41) ||  // page up/down, end, home, arrows
        (ev.keyCode > 111 && ev.keyCode < 124);  // fn keys

    return !anyNonPrintable && !(ev.charCode == 0 && mozNonPrintable);
  }
  /* eslint-enable complexity, eqeqeq */

  /**
   * Returns true if `value` is valid. The validator provided in `validator` will be used first, then any constraints.
   * @return {boolean} True if the value is valid.
   */
  validate() {
    // Use the nested input's native validity.
    let valid = this._formElement.checkValidity();

    // Only do extra checking if the browser thought this was valid.
    if (valid) {
      // Empty, required input is invalid
      if (this.required && this.value === '') {
        valid = false;
      } else if (this.hasValidator()) {
        valid = super.validate(this.value);
      }
    }

    this.invalid = !valid;
    /**
     * Fired whenever `validate()` is called.
     * @event cells-input-validate
     */
    this.dispatchEvent(new CustomEvent('cells-input-validate'));
    return valid;
  }
}

customElements.define(CellsInput.is, CellsInput);
