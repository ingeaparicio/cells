![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

# \<cells-textarea>

`CellsTextarea` provides a base class as well as a `<cells-textarea>` custom element with textarea functionality as close as possible to native textareas. It provides an easier and convenient way to build textareas with encapsulated HTML & custom styles. As it almost does not provide any styles, it's specially aimed to be extended.

As native textareas, it can communicate with forms in the same document, as well as use aria attributes (for example, aria-haspopup) for relating with other elements in the same document. It can receive a 'disabled' state, receive an 'autofocus' attribute to get focus when component is attached to the document, and more.

`CellsTextarea` is inspired on [weightless.dev Textarea](https://weightless.dev/elements/textarea) for the form communication, and on [Iron Input](https://www.webcomponents.org/element/@polymer/iron-input) for validations management.

When extending from `CellsTextarea`, just make sure to:

- Call `CellsTextarea` styles using `super.styles`
- Interpolate `this.labelledTextarea` in render.

**Example**

```js
// Extending CellsTextarea to create a CustomTextarea element
class CustomTextarea extends CellsTextarea {
  static get properties() {
    return {
      // Additional properties here
    };
  }

  static get styles() {
    // be sure to get styles from upper class using super
    return [
      super.styles,
      styles,
      getComponentSharedStyles('custom-textarea-shared-styles')
    ];
  }

  render() {
    // include any HTML you need; for example, icons
    return html`
      ${this.labelledTextarea}
    `;
  }
}
customElements.define('custom-textarea', CustomTextarea);
```

## Custom validators

`CellsTextarea` extends `CellsValidatableMixin`. You can use custom validators that implement `CellsValidatorMixin` with `<cells-textarea>`.

```html
<cells-textarea auto-validate validator="my-custom-validator"></cells-textarea>
```

## Examples

```html
<cells-textarea label="Default"></cells-textarea>
<cells-textarea label="Placeholder" placeholder="Message..."></cells-textarea>
<cells-textarea label="Readonly:" readonly></cells-textarea>
<cells-textarea label="Disabled" disabled></cells-textarea>
<cells-textarea label="Autofocus" autofocus=></cells-textarea>
<cells-textarea label="Must be between 5 and 10 characters" auto-validate required maxlength="10" minlength="5"></cells-textarea>
```
