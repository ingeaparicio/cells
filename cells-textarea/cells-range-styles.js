import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  position: relative;
  outline: none; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

:host([aria-disabled=true]) {
  outline: none;
  opacity: .4; }

.container {
  position: relative;
  display: flex;
  justify-content: center;
  --_knob-left: 0; }

.bar {
  position: absolute;
  top: 50%;
  left: 0;
  height: 1rem;
  display: flex;
  align-items: center;
  width: 100%;
  overflow: hidden;
  transform: translateY(-50%); }
  .bar-filled {
    position: absolute;
    right: calc(100% - 1rem / 2);
    transform: translateX(var(--_knob-left));
    width: calc(100% - 1rem);
    height: .25rem;
    background-color: #D3D3D3; }
    .bar-filled::before {
      content: '';
      position: absolute;
      top: 0;
      right: 100%;
      height: 100%;
      width: 1rem;
      background-color: inherit; }
  .bar::before {
    content: '';
    flex: auto;
    height: .25rem;
    background-color: #D3D3D3; }

.current {
  position: relative;
  transform: translateX(var(--_knob-left));
  width: calc(100% - 1rem); }

.knob {
  width: 1rem;
  height: 1rem;
  border-radius: 50%;
  position: relative;
  border: 1px solid #FFF;
  background-color: #1973B8;
  transform: translateX(-50%);
  cursor: pointer; }
  :host([aria-disabled=true]) .knob,
  :host([aria-readonly=true]) .knob {
    cursor: default; }
  :host([focus-visible]:not([aria-disabled=true])) .knob {
    box-shadow: 0 0 4px #043a63; }
  :host([aria-readonly=true]) .knob {
    background-color: #666666; }

.slot ::slotted(*) {
  position: absolute;
  bottom: 0;
  left: 0;
  z-index: -1;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: flex-end;
  margin: 0;
  opacity: 0;
  pointer-events: none; }
`;